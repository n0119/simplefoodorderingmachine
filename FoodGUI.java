import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class FoodGUI {

    private JPanel root;

    private JLabel topLabel;
    private JButton tempuraButton;
    private JButton ramenButton;
    private JButton udonButton;
    private JTextPane textPane1;
    private JButton gyudonButton;
    private JButton unajyuButton;
    private JButton misosoupButton;
    private JButton checkoutButton;
    private JLabel total;

    int a = 0;

    void order(String food, int sum) {
        int confirmation = JOptionPane.showConfirmDialog(null,
                "Would you like to order " + food + "?",
                "Order Confirmation", JOptionPane.YES_NO_OPTION);

        if (confirmation == 0) {
            JOptionPane.showMessageDialog(null, "Thank you for ordering " + food + "! It will be served as soon as possible.\n");

            String currentText = textPane1.getText();
            textPane1.setText(currentText + food + " " + sum + "yen" + "\n");
            a += sum;
            total.setText("Total  " + a + "yen");
        }
    }

    public static void main(String[] args) {

        JFrame frame = new JFrame("FoodGUI");
        frame.setContentPane(new FoodGUI().root);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.pack();
        frame.setVisible(true);

    }

    public FoodGUI() {

        tempuraButton.addActionListener(new ActionListener() {
            @Override

            public void actionPerformed(ActionEvent e) {
                order("Tempura", 1000);
            }

        });

        tempuraButton.setIcon(new ImageIcon( this.getClass().getResource("tempura.png")));;


        ramenButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order("Ramen", 800);

            }
        });

        ramenButton.setIcon(new ImageIcon(this.getClass().getResource("ramen.png")));
        ;


        udonButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order("Udon", 300);
            }
        });
        udonButton.setIcon(new ImageIcon(this.getClass().getResource("udon.png")));
        ;

        gyudonButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order("gyudon", 500);

            }
        });
        gyudonButton.setIcon(new ImageIcon(this.getClass().getResource("gyudon.png")));
        ;

        unajyuButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order("unajyu", 1200);

            }
        });
        unajyuButton.setIcon(new ImageIcon(this.getClass().getResource("unajyu.png")));
        ;

        misosoupButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order("misosoup", 100);

            }
        });
        misosoupButton.setIcon(new ImageIcon(this.getClass().getResource("misosoup.png")));
        ;

        checkoutButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                int confirmation = JOptionPane.showConfirmDialog(null,
                        "would you like to checkout? ",
                        "Order confirmation",
                        JOptionPane.YES_NO_OPTION);

                if (confirmation == 0) {
                    textPane1.setText(null);
                    JOptionPane.showMessageDialog(null, "Thank you .The total price is "
                            + a + " yen.");
                    a = 0;
                    total.setText("Total  " + a + "yen");
                }
            }
        });
    }

}
